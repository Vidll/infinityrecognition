using System.Collections.Generic;
using UnityEngine;

public class InfinityRecognition : MonoBehaviour
{
	private Noedify.Net net;
	private Noedify_Solver solver;

	public Paint Paint;

	public float TrainingRate = 0.4f;

	public int NoEpochs = 10;
	public int BatchSize = 1;
	public int NoLabels = 10;
	public int HiddenLayerNeurons = 10;

	public Noedify_Solver.CostFunction costFunction;

	public Texture2D[] TrainImage1;
	public Texture2D[] TrainImage2;
	public Texture2D[] TrainImage3;
	public Texture2D[] TrainImage4;
	public Texture2D[] TrainImage5;
	public Texture2D[] TrainImage6;
	public Texture2D[] TrainImage7;

	private void Start()
	{
		CreateModel();
		TrainingModel();
		GlobalEvents.SetResult?.Invoke("0");
	}

	private void CreateModel()
	{
		net = new Noedify.Net();

		Noedify.Layer inputLayer = new Noedify.Layer(
			Noedify.LayerType.Input2D,
			new int[2] {64, 64},
			1,
			"input layer");

		Noedify.Layer hiddenLayer0 = new Noedify.Layer(
			Noedify.LayerType.FullyConnected,
			HiddenLayerNeurons,
			Noedify.ActivationFunction.Sigmoid,
			"hidden layer 1");

		Noedify.Layer outLayer = new Noedify.Layer(
			Noedify.LayerType.Output,
			NoLabels,
			Noedify.ActivationFunction.SoftMax,
			"output layer");

		net.AddLayer(inputLayer);
		net.AddLayer(hiddenLayer0);
		net.AddLayer(outLayer);

		net.BuildNetwork();
	}

	[ContextMenu("Training model")]
	private void TrainingModel()
	{
		List<float[,,]> trainingData = new List<float[,,]>();
		List<float[]> outputData = new List<float[]>();

		List<Texture2D[]> MNISTimages = new List<Texture2D[]> 
		{ TrainImage1, TrainImage2, TrainImage3, TrainImage4 };

		Noedify_Utils.ImportImageData(ref trainingData, ref outputData, MNISTimages, true);

		Noedify_Solver.SolverMethod solverMethod = Noedify_Solver.SolverMethod.MainThread;

		if (solver == null)
			solver = Noedify.CreateSolver();
		solver.debug = new Noedify_Solver.DebugReport();

		solver.TrainNetwork(net, trainingData, outputData, NoEpochs, BatchSize, TrainingRate, costFunction, solverMethod, null, 8);
	}

	private void CheckPaintImage()
	{
		float[,,] testInputImage = new float[1, 1, 1];
		Noedify_Utils.ImportImageData(ref testInputImage, Paint.GetPaintTexture, false);//
		solver.Evaluate(net, testInputImage);
		SetResult();
	}

	private void SetResult()
	{
		var pred = 0f;

		foreach (var value in solver.prediction)
		{
			//print("Prediction:" + value);
			pred = +value;
		}

		pred = pred * 1000;
		if (pred > 2)
		{
			GlobalEvents.SetResult?.Invoke("Yes");
		}
		else
		{
			GlobalEvents.SetResult?.Invoke("No");
		}
	}

	private void OnEnable()
	{
		GlobalEvents.ClickOnCheckButton += CheckPaintImage;
	}

	private void OnDisable()
	{
		GlobalEvents.ClickOnCheckButton -= CheckPaintImage;
	}
}
