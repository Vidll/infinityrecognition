using UnityEngine;

public class SaveImageButton : MyButton
{
	public override void ClickOnButton()
	{
		base.ClickOnButton();
		GlobalEvents.ClickOnSaveImage?.Invoke();
	}
}
