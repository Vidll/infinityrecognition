using UnityEngine;

public class CheckButton : MyButton
{
	public override void ClickOnButton()
	{
		base.ClickOnButton();
		GlobalEvents.ClickOnCheckButton?.Invoke();
	}
}
