using UnityEngine;

public static class GlobalEvents 
{
    public delegate void UIEvent();
    public delegate void UIEventSetText(string text);

    public static UIEvent ClickOnCheckButton;
	public static UIEvent ClickOnClearButton;
	public static UIEventSetText SetResult;

    //Training network
    public static UIEvent ClickOnSaveImage;
}
