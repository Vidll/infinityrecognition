using UnityEngine;
using System.IO;

public class Paint : MonoBehaviour
{
	[SerializeField] private TextureWrapMode _wrapMode;
	[SerializeField] private FilterMode _filterMode;

	[SerializeField] private Texture2D _texture2d;
	[SerializeField] private Material _material;
	[SerializeField] private Camera _camera;
	[SerializeField] private Collider _collider;

	[SerializeField]private int _textureSize = 128;

	private Color _drawColor = Color.white;
	private float _rayLength = 100f;
	private int _imageCount = 1;

	public Texture2D GetPaintTexture { get => _texture2d; }

	private void Start()
	{
		if (!_camera)
			_camera = Camera.main;
		if (!_collider)
			_collider = GetComponent<Collider>();

		SetColor(Color.black);
	}

	private void OnEnable()
	{
		Subscribe();

	}

	private void Update()
	{
		if (Input.GetMouseButton(0))
			Drawing();
	}

	private void OnValidate()
	{
		if (_texture2d == null)
			_texture2d = new Texture2D(_textureSize, _textureSize);
		
		_texture2d.Reinitialize(_textureSize, _textureSize);
		SetColor(Color.black);
		_texture2d.wrapMode = _wrapMode;
		_texture2d.filterMode = _filterMode;
		_material.mainTexture = _texture2d;
		_texture2d.Apply();
	}

	private void SetColor(Color color)
	{
		for(int n1 = 0; n1 < _textureSize; n1++)
			for(int n2 = 0; n2 < _textureSize; n2++)
				_texture2d.SetPixel(n1, n2, color);
	}

	private void Drawing()
	{
		Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if (_collider.Raycast(ray, out hit, _rayLength))
		{
			int rayX = (int)(hit.textureCoord.x * _textureSize);
			int rayY = (int)(hit.textureCoord.y * _textureSize);
			_texture2d.SetPixel(rayX, rayY, _drawColor);
			_texture2d.Apply();
		}
	}

	private void CreateNewTexture()
	{
		_texture2d = new Texture2D( _textureSize, _textureSize);
		_texture2d.Reinitialize(_textureSize, _textureSize);

		_texture2d.wrapMode = _wrapMode;
		_texture2d.filterMode = _filterMode;
		_material.mainTexture = _texture2d;

		SetColor(Color.black);
		_texture2d.Apply();
	}

	private void SavePicture()
	{
		byte[] textureBytes = _texture2d.EncodeToPNG();
		var path = Application.dataPath;
		File.WriteAllBytes(path + "Image" + _imageCount + ".png", textureBytes);
		_imageCount++;
		CreateNewTexture();
	}

	private void Subscribe()
	{
		GlobalEvents.ClickOnSaveImage += SavePicture;
		GlobalEvents.ClickOnClearButton += CreateNewTexture;
	}

	private void Unsubscribe()
	{
		GlobalEvents.ClickOnSaveImage -= SavePicture;
		GlobalEvents.ClickOnClearButton -= CreateNewTexture;
	}

	private void OnDisable()
	{
		Unsubscribe();
	}
}
