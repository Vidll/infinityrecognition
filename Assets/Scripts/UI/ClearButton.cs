using UnityEngine;

public class ClearButton : MyButton
{
	public override void ClickOnButton()
	{
		base.ClickOnButton();
		GlobalEvents.ClickOnClearButton?.Invoke();
	}
}
