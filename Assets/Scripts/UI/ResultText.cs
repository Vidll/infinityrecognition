using UnityEngine;
using TMPro;

public class ResultText : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _text;

	private void OnEnable()
	{
		_text = GetComponent<TextMeshProUGUI>();
		GlobalEvents.SetResult += ChangeText;
	}

	private void OnDisable()
	{
		GlobalEvents.SetResult -= ChangeText;
	}

	private void ChangeText(string content)
	{
		_text.text = "Prediction:" + content;
	}
}
