using UnityEngine;
using UnityEngine.UI;

public abstract class MyButton : MonoBehaviour
{
	private Button _button;

	private void OnEnable()
	{
		_button = GetComponent<Button>();
		_button.onClick.AddListener(ClickOnButton);
	}

	private void OnDisable()
	{
		_button.onClick.RemoveListener(ClickOnButton);
	}

	public virtual void ClickOnButton() { }
}
